const Analytics = require('../analytics');
const messages = require('../constants/messages');

module.exports = function () {
	// Alexa, ask OneSphere how much did Brian spend this month
	//
	// RE: AUDIO  Your amazon order of Star Wars
	// Darth Vader Voice Changer Helmet with Lightsaber is out for delivery.

	console.log('* twistIntent');

	const analytics = new Analytics();
	analytics.sendAsset('TWIST');

	this.attributes.speechOutput = this.t('TWIST_RESPONSE');
	this.response.speak(this.attributes.speechOutput).listen(this.t(messages.DefaultRepromt));
	this.emit(messages.ResponseReady);
};
