const { onesphereClient } = require('../api');
const Analytics = require('../analytics');
const messages = require('../constants/messages');

module.exports = function () {
	// Alexa, what is the status of my zones in Houston region?
	// Alexa, ask One Sphere about the status of my zones in Houston region?
	// or Alexa, how is my Houston region doing?
	// or Alexa, how is Houston doing?
	//
	// RE: In Houston region there is 1 zone:
	// Beach-1 zone is OK and; it has 1 cluster named Picaso and 8 datastores and 5 networks.

	console.log('* ZoneStatusByRegionIntent');
	const intentObj = this.event.request.intent;

	if (intentObj.slots.Region.confirmationStatus !== messages.confirmed) {
		if (intentObj.slots.Region.confirmationStatus !== messages.Denied &&
			intentObj.slots.Region.value) {
			const slotToConfirm = 'Region';
			const speechOutput = this.t('REGION_CONFIRMATION', intentObj.slots.Region.value);
			const repromptSpeech = speechOutput;
			this.emit(':confirmSlot', slotToConfirm, speechOutput, repromptSpeech);
		} else {
			const slotToElicit = 'Region';
			const speechOutput = this.t('REGION_QUESTION');
			const repromptSpeech = speechOutput;
			this.emit(':elicitSlot', slotToElicit, speechOutput, repromptSpeech);
		}
	} else {
		const analytics = new Analytics();
		analytics.sendAsset('ZoneStatusByRegionIntent');

		let regionName = intentObj.slots.Region.value.toLowerCase();

		if (intentObj.slots.Region.resolutions
			&& intentObj.slots.Region.resolutions.resolutionsPerAuthority
			&& intentObj.slots.Region.resolutions.resolutionsPerAuthority.length > 0
			&& intentObj.slots.Region.resolutions.resolutionsPerAuthority[0].values
			&& intentObj.slots.Region.resolutions.resolutionsPerAuthority[0].values.length > 0
			&& intentObj.slots.Region.resolutions.resolutionsPerAuthority[0].values[0].value
			&& intentObj.slots.Region.resolutions.resolutionsPerAuthority[0].values[0].value.id) {
			regionName = intentObj.slots.Region.resolutions.resolutionsPerAuthority[0].values[0].value.id.toLowerCase();
		}

		onesphereClient.getRegionsStatus(this.event.session.authData).then((resp) => {
			const region = resp
				.members.find(r => r.name.toLowerCase() === regionName);

			if (region) {
				if (region.zones && region.zones.length > 0) {
					const execute = [];

					region.zones.forEach((zone) => {
						execute.push(onesphereClient.getEntityDetails(this.event.session.authData, zone.uri)
							.then((zoneDetails) => {
								let output = `${zone.name}, `;

								if (zoneDetails.clusters.length > 0) {
									output += ` that has ${zoneDetails.clusters.length} `;

									zoneDetails.clusters.forEach((cluster) => {
										output += `cluster named "${cluster.name}"`;
										if (cluster.datastores.length > 0) {
											output += ` with ${cluster.datastores.length} datastores, `;
										}
									});
								}

								return output;
							}).then((clusterInfo) => {
								return onesphereClient.getNetworks(this.event.session.authData, `zoneUri EQ ${zone.uri}`)
									.then(zoneNetworks => `${clusterInfo} and ${zoneNetworks.total} networks.`)
									.catch(() => clusterInfo);
							}));
					});

					return Promise.all(execute).then((clusters) => {
						if (region.zones.length === 1) {
							this.response.speak(this.t('REGION_STATUS_FULL_SINGLE', intentObj.slots.Region.value, region.status, region.zones.length, clusters.join(' cluster '))).listen(this.t(messages.DefaultRepromt));
						} else {
							this.response.speak(this.t('REGION_STATUS_FULL_PLURAL', intentObj.slots.Region.value, region.status, region.zones.length, clusters.join(' cluster '))).listen(this.t(messages.DefaultRepromt));
						}
						this.emit(messages.ResponseReady);
					});
				}
				this.response.speak(this.t('REGION_STATUS_SHORT', intentObj.slots.Region.value, region.status)).listen(this.t(messages.DefaultRepromt));
				this.emit(messages.ResponseReady);
			} else {
				this.response.speak(this.t('REGION_NOT_FOUND', intentObj.slots.Region.value)).listen(this.t(messages.DefaultRepromt));
				this.emit(messages.ResponseReady);
			}
			return null;
		})
			.catch((err) => {
				console.log(err);
				this.attributes.speechOutput = this.t(messages.ErrorMessage);
				this.response.speak(this.attributes.speechOutput).listen(this.t(messages.DefaultRepromt));
				this.emit(messages.ResponseReady);
			});
	}
};
