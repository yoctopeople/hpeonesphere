const { onesphereClient } = require('../api');
const Analytics = require('../analytics');
const messages = require('../constants/messages');
const deployemntId = process.env.DEPLOYMENT_ID || '081d93ea-84bb-4fdb-b30f-5a856f07e84e';

module.exports = function () {
	// Alexa, shutdown my project named "Executive Dashboard"
	//
	// RE OK, Bryan, the project Executive Dashboard will become inactive if you suspend it.
	// Do you want to proceed?
	//
	// Yes!
	//
	// RE: OK, suspending project…

	console.log('* ProjectShutdownIntent');
	const intentObj = this.event.request.intent;

	if (intentObj.slots.ProjectName.confirmationStatus !== messages.confirmed) {
		if (intentObj.slots.ProjectName.confirmationStatus !== messages.Denied) {
			const slotToConfirm = 'ProjectName';
			const speechOutput = this.t('PROJECT_SHUTDOWN_CONFIRMATION', intentObj.slots.ProjectName.value);
			const repromptSpeech = speechOutput;
			this.emit(':confirmSlot', slotToConfirm, speechOutput, repromptSpeech);
		} else {
			const slotToElicit = 'ProjectName';
			const speechOutput = this.t('PROJECT_SHUTDOWN_QUESTION');
			const repromptSpeech = speechOutput;
			this.emit(':elicitSlot', slotToElicit, speechOutput, repromptSpeech);
		}
	} else {
		const analytics = new Analytics();
		analytics.sendAsset('ProjectShutdownIntent');
		onesphereClient.projectShutdown(this.event.session.authData, deployemntId)
			.then(() => {
				console.log('project suspended');
			})
			.catch(() => {
				console.log('cannot shutdown project');
			})
			.then(() => {
				this.attributes.speechOutput = this.t('PROJECT_SHUTDOWN_SUCCESS');
				this.response.speak(this.attributes.speechOutput).listen(this.t(messages.DefaultRepromt));
				this.emit(messages.ResponseReady);
			});
	}
};
