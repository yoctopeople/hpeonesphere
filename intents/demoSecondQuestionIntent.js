const Analytics = require('../analytics');
const messages = require('../constants/messages');

module.exports = function () {
	// Alexa, you can answer Ric what you think of OneSphere?
	//
	// RE: I must say
	// I’m enjoying my interactions with OneSphere
	// and recommend every AWS customer sign up today, Ric!

	console.log('* DemoSecondQuestionIntent');

	const analytics = new Analytics();
	analytics.sendAsset('DemoSecondQuestionIntent');

	this.attributes.speechOutput = this.t('DEMO_QUESTION_2_RESPONSE');
	this.response.speak(this.attributes.speechOutput).listen(this.t(messages.DefaultRepromt));
	this.emit(messages.ResponseReady);
};
