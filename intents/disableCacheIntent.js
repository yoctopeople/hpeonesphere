const CacheManager = require('../cacheManager');
const messages = require('../constants/messages');

module.exports = async function () {
	// Alexa, disable cache
	//
	// Done
	console.log('* DisableCacheIntent');
	try {
		const cacheManager = new CacheManager();
		await cacheManager.disableCache();
		this.attributes.speechOutput = 'Disabled!';
		this.response.speak(this.attributes.speechOutput).listen(this.t(messages.DefaultRepromt));
		this.emit(messages.ResponseReady);
	} catch (err) {
		console.log(err);
		this.attributes.speechOutput = this.t(messages.ErrorMessage);
		this.response.speak(this.attributes.speechOutput).listen(this.t(messages.DefaultRepromt));
		this.emit(messages.ResponseReady);
	}
};
