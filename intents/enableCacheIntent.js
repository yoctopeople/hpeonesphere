const CacheManager = require('../cacheManager');
const messages = require('../constants/messages');

module.exports = async function () {
	// Alexa, enable cache
	//
	// Cache enabled!
	console.log('* EnableCacheIntent');
	try {
		const cacheManager = new CacheManager();
		await cacheManager.exec(this.event.session.authData);
		this.attributes.speechOutput = 'Enabled!';
		this.response.speak(this.attributes.speechOutput).listen(this.t(messages.DefaultRepromt));
		this.emit(messages.ResponseReady);
	} catch (err) {
		console.log(err);
		this.attributes.speechOutput = this.t(messages.ErrorMessage);
		this.response.speak(this.attributes.speechOutput).listen(this.t(messages.DefaultRepromt));
		this.emit(messages.ResponseReady);
	}
};
