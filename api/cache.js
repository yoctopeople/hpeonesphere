const server = process.env.CACHE_SERVER || 'http://alexa-cache.yoctocode.com:8877/api/v1';
const request = require('superagent');

class Cache {
	getKey(method, params) {
		if (method === 'session') {
			const host = params[0];
			return `session_${host}`;
		}

		const paramsKey = JSON.stringify(params);
		return `${method}_${paramsKey}`;
	}

	async getValue(method, params) {
		const key = this.getKey(method, params);

		let response = null;
		try {
			const isCacheEnabledResponse = await request.get(`${server}/state`);
			if (isCacheEnabledResponse.body.success === true && isCacheEnabledResponse.body.data === true) {
				const cachedData = await request
					.get(`${server}/cache`)
					.query({ key });

				if (cachedData.body.success === true) {
					response = cachedData.body.data;
				}
			}
		} catch (e) {
			console.log('get cache error', e);
		}


		return response;
	}

	async updateValue(method, params, value) {
		const key = this.getKey(method, params);
		const payload = {
			value,
		};

		let result = null;
		try {
			const isCacheEnabledResponse = await request.get(`${server}/state`);
			if (isCacheEnabledResponse.body.success === true && isCacheEnabledResponse.body.data === true) {
				const response = await request
					.post(`${server}/cache`)
					.query({ key })
					.send(payload);
				result = response.body.data;
			}
		} catch (e) {
			console.log('update cache error', e);
		}

		return result;
	}

	updateAuthData(authData) {
		const payload = {
			value: authData,
		};
		return request
			.post(`${server}/cache`)
			.query({ key: `session_${authData.host}` })
			.send(payload)
			.then((response) => {
				return response.body.data;
			});
	}

	async enableAndClean() {
		await request.post(`${server}/clean`);
		await request.post(`${server}/state/true`);
	}

	async disableAndClean() {
		await request.post(`${server}/clean`);
		await request.post(`${server}/state/false`);
	}
}

module.exports = Cache;