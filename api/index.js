const OneSpehreClient = require('./onesphere-client');
const OneSphereApi = require('./onesphere-api');
const Cache = require('./cache');

const onesphereApi = new OneSphereApi();
const cache = new Cache();

const onesphereClient = new OneSpehreClient(onesphereApi, cache);


module.exports = {
	onesphereClient,
	cache,
};
