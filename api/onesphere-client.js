const cacheEnabled = process.env.USE_CACHE || true;

class OneSphereClient {
	constructor(oneSphereApi, cache) {
		this.oneSphereApi = oneSphereApi;
		this.cache = cache;
	}

	async executeMethod(method, ...args) {
		let response = null;
		if (cacheEnabled) {
			try {
				response = await this.cache.getValue(method.name, args);
			} catch (e) {
				console.log(e);
			}
		}

		if (!response) {
			response = await method(...args);
			if (cacheEnabled) {
				await this.cache.updateValue(method.name, args, response);
			}
		}

		return response;
	}

	session(host, username, password) {
		return this.executeMethod(this.oneSphereApi.session, host, username, password);
	}

	getUser(authData) {
		return this.executeMethod(this.oneSphereApi.getUser, authData);
	}

	getProvidersList(authData) {
		return this.executeMethod(this.oneSphereApi.getProvidersList, authData);
	}

	projectShutdown(authData, projectId) {
		return this.executeMethod(this.oneSphereApi.projectShutdown, authData, projectId);
	}

	getProjectsList(authData) {
		return this.executeMethod(this.oneSphereApi.getProjectsList, authData);
	}

	getProviderRating(authData) {
		return this.executeMethod(this.oneSphereApi.getProviderRating.bind(this.oneSphereApi), authData);
	}

	getRegionsStatus(authData) {
		return this.executeMethod(this.oneSphereApi.getRegionsStatus, authData);
	}

	getEntityDetails(authData, entityUri) {
		return this.executeMethod(this.oneSphereApi.getEntityDetails, authData, entityUri);
	}

	getNetworks(authData, query) {
		return this.executeMethod(this.oneSphereApi.getNetworks, authData, query);
	}

	getTotalSpend(authData, params) {
		return this.executeMethod(this.oneSphereApi.getTotalSpend, authData, params);
	}

	getUtilization(authData, resourceUri) {
		return this.executeMethod(this.oneSphereApi.getUtilization.bind(this.oneSphereApi), authData, resourceUri);
	}
}

module.exports = OneSphereClient;