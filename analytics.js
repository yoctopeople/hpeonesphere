const request = require('superagent');

const ClientId = process.env.ANALYTICS_CLIENT_ID;
const ApiUri = process.env.ANALYTICS_API_URI;

class Analytics {
	// eslint-disable-next-line class-methods-use-this
	async sendAsset(assetTitle) {
		const payload = {
			key: 'click',
			value: {
				clientId: ClientId,
				category: 'Alexa OneSphere',
				asset: {
					title: `alexa: ${assetTitle}`,
					link: 'https://alexa.onesphere.yoctocode.com',
				},
			},
		};

		try {
			await request
				.post(`${ApiUri}/analytics/analytics`)
				.send(payload)
				.set('Accept', 'application/json');
		} catch (e) {
			console.log('send analytics failed', e);
		}
	}
}

module.exports = Analytics;
