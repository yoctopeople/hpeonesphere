const launchRequst = require('./intents/LaunchRequest');
const lobSpendCurrentMonthIntent = require('./intents/lobSpendCurrentMonthIntent');
const projectShutdownIntent = require('./intents/projectShutdownIntent');
const projectsTopSpendCurrentMonthIntent = require('./intents/projectsTopSpendCurrentMonthIntent');
const projectsTotalSpendCurrentMonthIntent = require('./intents/projectsTotalSpendCurrentMonthIntent');
const providerTypesExtendedStatusIntent = require('./intents/providerTypesExtendedStatusIntent');
const totalSpendAcrossProvidersIntent = require('./intents/totalSpendAcrossProvidersIntent');
const regionStatusIntent = require('./intents/regionStatusIntent');
const utilizationAcrossHybridCloudsIntentByProviderType = require('./intents/utilizationAcrossHybridCloudsIntentByProviderType');
const zoneStatusByRegionIntent = require('./intents/zoneStatusByRegionIntent');
const translations = require('./translations');
const messages = require('./constants/messages');
const { cache } = require('./api');


class CacheManager {
	constructor() {
		this.attributes = {
			speechOutput: '',
			repromptSpeech: '',
		};
		this.event = {
			session: {},
			request: {
				intent: {
					slots: {
						ProjectName: {},
						Month: {},
						Provider: {},
						Region: {},
					},
				},
			},
		};

		this.response = {
			speak: (speakMessage) => {
				this.attributes.speechOutput = speakMessage;
				return {
					listen: (listenMessage) => {
						this.attributes.repromptSpeech = listenMessage;
					},
				};
			},
		};
	}

	t(key) {
		return translations.en.translation[key];
	}

	executeIntent(intent) {
		return new Promise((resolve, reject) => {
			this.emit = () => {
				if (this.attributes.speechOutput === this.t(messages.ErrorMessage)) {
					// TODO: maybe it's better to ignore this
					reject();
				} else {
					resolve();
				}
			};
			const command = intent.bind(this);
			command();
		});
	}

	async exec(authData) {
		await cache.enableAndClean();

		this.event.session.authData = authData;
		await cache.updateAuthData(authData);

		await this.executeIntent(launchRequst);
		await this.executeIntent(lobSpendCurrentMonthIntent);
		// Project ID?
		// this.event.request.intent.slots.ProjectName.confirmationStatus = messages.confirmed;
		// await this.executeIntent(projectShutdownIntent);
		await this.executeIntent(projectsTopSpendCurrentMonthIntent);
		await this.executeIntent(projectsTotalSpendCurrentMonthIntent);
		await this.executeIntent(providerTypesExtendedStatusIntent);

		await this.executeIntent(totalSpendAcrossProvidersIntent);

		this.event.request.intent.slots.Provider.confirmationStatus = messages.confirmed;
		this.event.request.intent.slots.Provider.value = 'Private Cloud';
		await this.executeIntent(utilizationAcrossHybridCloudsIntentByProviderType);
		await this.executeIntent(regionStatusIntent);
		this.event.request.intent.slots.Provider.value = 'Amazon Web Services';
		await this.executeIntent(utilizationAcrossHybridCloudsIntentByProviderType);
		await this.executeIntent(regionStatusIntent);

		this.event.request.intent.slots.Region.confirmationStatus = messages.confirmed;

		// deic
		this.event.request.intent.slots.Region.value = 'us-west-2';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'emea-mougins-fr';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'svt-hou';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'eu-west-1';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'ap-northeast-2';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'us-west-2';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'us-east-2';
		await this.executeIntent(zoneStatusByRegionIntent);

		/*
		// cic
		this.event.request.intent.slots.Region.value = 'GVA';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'Geneva';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'eu-central-1';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'Houston';
		await this.executeIntent(zoneStatusByRegionIntent);
		this.event.request.intent.slots.Region.value = 'us-west-1';
		await this.executeIntent(zoneStatusByRegionIntent);
		*/

		console.log(authData);
	}

	async disableCache() {
		return cache.disableAndClean();
	}
}

module.exports = CacheManager;
